## Uasset Data Extractor

This program is able to parse blueprint assets and maps of most games built with Unreal Engine 4.

Note however that there is limited support for all of the properties that can be serialized, and the parser may panic if it attempts to parse an unknown tag type.

### Usage
```yaml
USAGE:
    uasset-data-extractor [OPTIONS] --asset <asset> [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
        --asset <asset>      Sets the input file to use
        --output <output>    Sets the input file to use
        --pkgver <pkgver>    Sets the input file to use
        --uexp <uexp>        Sets the input file to use

SUBCOMMANDS:
    dump      exports data values from an asset to stdout
    export    exports data values from an asset
    help      Prints this message or the help of the given subcommand(s)
```

### Differences
This fork:
* is primarily focused on supporting pre-4.20 Unreal Engine versions too.
* is focused only on blueprint data. Texture, mesh, animations, PAK files support is removed.
* supports assets built without uexp files.
* has an incompatible CLI interface.