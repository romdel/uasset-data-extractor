extern crate byteorder;
extern crate hex;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
#[macro_use]
extern crate erased_serde;
#[macro_use]
extern crate clap;

use std::path::Path;
use std::fs;
use std::io::{Read, Write};
use clap::{Arg, App, SubCommand};

mod parser_settings;
mod decompress;
mod rijndael;
mod ue_version;
mod licensee_versions;
mod assets;
mod archives;

#[derive(Debug)]
struct CommandError {
    message: String,
}

impl std::error::Error for CommandError {

}

impl std::fmt::Display for CommandError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Error: {}", self.message)
    }
}

impl From<assets::ParserError> for CommandError {
    fn from(error: assets::ParserError) -> Self {
        let property_error = error.get_properties().into_iter().rev().fold(String::new(), |acc, v| acc + "\n" + v);
        CommandError {
            message: "Property error occurred: ".to_owned() + &property_error,
        }
    }
}

impl From<std::io::Error> for CommandError {
    fn from(error: std::io::Error) -> CommandError {
        CommandError {
            message: format!("File Error: {}", error),
        }
    }
}

impl From<clap::Error> for CommandError {
    fn from(error: clap::Error) -> CommandError {
        CommandError {
            message: format!("{}", error),
        }
    }
}

type CommandResult = Result<(), CommandError>;

fn main() {
    let matches = App::new("Uasset Data Extractor")
                          .version(crate_version!())
                          .author(crate_authors!())
                          .about("Fork of https://github.com/SirWaddles/JohnWickParse, adjusted to be more universal (and data focused).")
                          .arg(Arg::with_name("asset")
                               .long("asset")
                               .help("Sets the input file to use")
                               .takes_value(true)
                               .required(true))
                          .arg(Arg::with_name("output")
                               .long("output")
                               .help("Sets the output file")
                               .takes_value(true)
                               .required(false))
                          .arg(Arg::with_name("uexp")
                               .long("uexp")
                               .help("Sets the input exports file to use")
                               .takes_value(true)
                               .required(false))
                          .arg(Arg::with_name("pkgver")
                               .long("pkgver")
                               .help("Forces a specific package version, ignoring the one supplied by the file.")
                               .takes_value(true)
                               .required(false))
                          .arg(Arg::with_name("exclude-type")
                               .long("exclude-type")
                               .help("Excludes a type from being read")
                               .takes_value(true)
                               .multiple(true)
                               .required(false))
                          .subcommand(SubCommand::with_name("export")
                                      .about("exports data values from an asset"))
                          .subcommand(SubCommand::with_name("dump")
                                      .about("exports data values from an asset to stdout"))
                          .get_matches();

    if let Some(_) = matches.subcommand_matches("export") {
        if let Err(error) = export_data(&matches) {
            println!("{}", error);
        }
    } else if let Some(_) = matches.subcommand_matches("dump") {
        if let Err(error) = dump_data(&matches) {
            println!("{}", error);
        }
    }
}

fn export_data(matches: &clap::ArgMatches) -> CommandResult {
    let uasset_path = value_t!(matches, "asset", String)?;
    let uexp_path = value_t!(matches, "uexp", String).unwrap_or("".to_string());
    let output_path = value_t!(matches, "output", String).unwrap();
    let file_version = value_t!(matches, "pkgver", i32).unwrap_or(-1);

    if file_version != -1 {
        println!("File version overriden: {}", file_version);
        assets::override_fileversion(file_version);
    }

    // read asset file
    let mut asset = fs::File::open(uasset_path)?;
    let mut uasset_buf = Vec::new();
    asset.read_to_end(&mut uasset_buf)?;

    // read uexp file
    let uexp_buf = if uexp_path != "" {
        let uexp_path_obj = Path::new(&uexp_path);
        match fs::metadata(uexp_path_obj).is_ok() {
            true => {
                let mut uexp = fs::File::open(uexp_path_obj)?;
                let mut uexp_tbuf = Vec::new();
                uexp.read_to_end(&mut uexp_tbuf)?;
                Some(uexp_tbuf)
            },
            false => {
                println!("Uexp file doesn't exist. Proceeding without.");
                None
            }
        }
    } else { None };
    
    let settings = parser_settings::ParserSettings {
        excluded_types: matches.values_of_lossy("exclude-type"),
    };

    let package = assets::Package::from_buffer(uasset_buf, uexp_buf, settings)?;
    let serial_package = serde_json::to_string(&package).unwrap();
    let mut file = fs::File::create(output_path).unwrap();
    file.write_all(serial_package.as_bytes()).unwrap();

    Ok(())
}

fn dump_data(matches: &clap::ArgMatches) -> CommandResult {
    let uasset_path = value_t!(matches, "asset", String)?;
    let uexp_path = value_t!(matches, "uexp", String).unwrap_or("".to_string());
    let file_version = value_t!(matches, "pkgver", i32).unwrap_or(-1);

    if file_version != -1 {
        println!("File version overriden: {}", file_version);
        assets::override_fileversion(file_version);
    }

    // read asset file
    let mut asset = fs::File::open(uasset_path)?;
    let mut uasset_buf = Vec::new();
    asset.read_to_end(&mut uasset_buf)?;

    // read uexp file
    let uexp_buf = if uexp_path != "" {
        let uexp_path_obj = Path::new(&uexp_path);
        match fs::metadata(uexp_path_obj).is_ok() {
            true => {
                let mut uexp = fs::File::open(uexp_path_obj)?;
                let mut uexp_tbuf = Vec::new();
                uexp.read_to_end(&mut uexp_tbuf)?;
                Some(uexp_tbuf)
            },
            false => {
                println!("Uexp file doesn't exist. Proceeding without.");
                None
            }
        }
    } else { None };
    
    let settings = parser_settings::ParserSettings {
        excluded_types: matches.values_of_lossy("exclude-type"),
    };

    let package = assets::Package::from_buffer(uasset_buf, uexp_buf, settings)?;
    println!("{:?}", package);

    Ok(())
}